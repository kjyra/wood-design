<footer class="site-footer layout-row">
				<span class="site-footer-text layout-col">
					20NN, Wood Design. Design by Tomas Laurinavičius.
				</span>
				<div class="layout-col">
					<div class="site-footer-links">
						<a href="#">
							<img src="img/flickr.jpg" alt= "Иконка">
							<span>Wood Design on Flickr</span>
						</a>
						<a href="#">
							<img src="img/twitter.jpg" alt= "Иконка">
							<span>Wood Design on Twitter</span>
						</a>
					</div>
				</div>
				<script src="js/jquery-3.4.1.min.js"></script>
				<script src="js/script.js"></script>
			</footer>
		</div>
		<?php wp_footer() ?>
	</body>
</html>