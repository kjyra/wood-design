<?php get_header() ?>
<?php 
function getShortText($text) {
	$str = mb_substr($text, 0, 35);
	if(iconv_strlen($str) > 30) {
		$str .= '...';
	}
	return $str;
}
?>
			<div class="content-wrapper layout-row">
					<main class="layout-col layout-col-main">
						<h1>Wood design - home page</h1>
						<div class="posts-flow layout-row">
						<?php while(have_posts()): ?>
						<?php the_post(); ?>
							<article class="post-card layout-col">
								<a href="<?= get_permalink() ?>" class="post-card-link">
									<img src="img/1st_Design.jpg" alt= "1st Design">
								</a>
								<h2 class="post-card-title"><?php the_title() ?></h2>
								<div class="post-card-intro"><?php echo getShortText(get_the_content()) ?></div>
							</article>
				<?php endwhile; ?>
						</div>
					</main>
				<aside class="layout-col layout-col-aside">
					<div class="aside-box">
						<div class="tw-wrapper">
							<div class="tw-inner">
								<div class="tw-text">
									<span>Free Wood Design PSD Template. For more freebies and photoshop tutorials follow @webdesignfan.</span>
								</div>
								<div class="tw-follow">
									<span>Follow Us on Twitter</span>
								</div>
							</div>
						</div>
					</div>
					<div class="aside-box">
						<!-- <div class="h2">Categories</div> -->
						<ul class="secondery-navigation">
							<?php dynamic_sidebar('sidabar-main') ?>
						</ul>
					</div>
					<div class="aside-box">
						<div class="h2">Contact Us</div>
						<ul class="contacts-list">
							<li>E-mail: hello@wp.dmitrylavrik.ru</li>
							<li>Phone: +111 111111 11 11</li>
							<li>Twitter: @noooooooooootwitter</li>
						</ul>
					</div>
				</aside>
			</div>
<?php get_footer() ?>