<!DOCTYPE html>
<html <?php language_attributes() ?>>
	<head>
		<title><?php the_title() ?></title>
		<meta charset="<?php bloginfo('charset')?>">
		<meta name="viewport" content="width=device-width">
		<base href="<?php echo get_template_directory_uri() . '/assets/'?>">
		<?php wp_head() ?>
	</head>
	<body>
		<div class="wrapper">
			<header>
				<div class="header-top">
					<div class="header-logo">
						<div class="title">Wood Design</div>
					</div>
					<nav class="header-menu">
						<div class="header-menu-button">Menu</div>
						<ul class="header-menu-list">
							<?php wp_nav_menu([
								'theme_location' => 'header',
								'containter' => false,
								'menu_class' => ''
							]) ?>
						</ul>
					</nav>
				</div>
				<div class="header-bottom">
					Wood Design is a modern web & graphic design studio in Europe. We create beautiful things for web and print. You can see our great work examples in <a href="/умения">Portfolio</a>. If you need a professional design services <a href="about">Contact</a> us. We would love to work with you.
				</div>
			</header>