<?php get_header() ?>
<div class="content-wrapper layout-row">
	<main class="layout-col layout-col-full">
		<h1><?php the_title() ?></h1>
		<p><?php the_content() ?></p>
	</main>
</div>
<?php get_footer() ?>