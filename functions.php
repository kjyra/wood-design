<?php

define('WP_CSS_DIR', get_template_directory_uri() . '/assets/css/');
define('WP_JS_DIR', get_template_directory_uri() . '/assets/js/');

add_action('wp_enqueue_scripts', function() {
	wp_enqueue_style('reset', WP_CSS_DIR . 'reset.css');
	wp_enqueue_style('styles', WP_CSS_DIR . 'styles.css');
	wp_enqueue_style('main', WP_CSS_DIR . 'main.css');
	wp_enqueue_script('jquery', WP_JS_DIR . 'jquery-3.4.1.min.js');
	wp_enqueue_script('script', WP_JS_DIR . 'script.js');
});

add_action('after_setup_theme', function() {
	register_nav_menu('header', 'Верхнее меню со страницами');
});

add_action( 'widgets_init', function() {
	register_sidebar( array(
			'name'          => 'Главный сайбар страницы',
			'id'            => 'sidabar-main',
			'description'   => 'выводит категории для главной страницы',
			// 'class'         => '',
			'before_widget' => '<li class="widget %2$s">',
			'after_widget'  => "</li>\n",
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => "</h2>\n",
			'before_sidebar' => '<div class="widget %2$s">', // WP 5.6
			'after_sidebar'  => '</div>', // WP 5.6
		) );

	register_sidebar( array(
		'name'          => 'правый верхний сайдбар для страницы одного поста',
		'id'            => 'sidabar-post-single-up',
		'description'   => 'выводит верхний правый сайдбар для одного поста',
		// 'class'         => '',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
		'before_sidebar' => '<div class="widget %2$s">', // WP 5.6
		'after_sidebar'  => '</div>', // WP 5.6
	) );

	register_sidebar( array(
		'name'          => 'правый нижний сайдбар для страницы одного поста',
		'id'            => 'sidabar-post-single-down',
		'description'   => 'выводит нижний правый сайдбар для одного поста',
		// 'class'         => '',
		'before_widget' => '<li class="widget %2$s">',
		'after_widget'  => "</li>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
		'before_sidebar' => '<div class="widget %2$s">', // WP 5.6
		'after_sidebar'  => '</div>', // WP 5.6
	) );
});