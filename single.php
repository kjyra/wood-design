<?php get_header() ?>
<?php the_post() ?>
<div class="content-wrapper layout-row">
				<main class="layout-col layout-col-main">
					<h1><?php the_title() ?></h1>
					<div class="post-full">
						<img src="img/1st_Design-full.jpg" alt= "1st Design">
						<p><?php the_content() ?></p>
					</div>
				</main>
				<aside class="layout-col layout-col-aside">
					<div class="aside-box">
						<div class="h2"><?php the_date() ?></div>
						<div class="h2">Author: <?php the_author() ?></div>
					</div>
					<div class="aside-box">
						<ul class="secondery-navigation">
							<?php dynamic_sidebar('sidabar-post-single-up') ?>
						</ul>
					</div>
					<div class="aside-box">
						<ul class="secondery-navigation">
							<?php dynamic_sidebar('sidabar-post-single-down') ?>
						</ul>
					</div>
				</aside>
			</div>
<?php get_footer() ?>